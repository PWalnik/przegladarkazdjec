﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PrzegladarkaZdjec
{
    public partial class s : Form
    {
        private string pathToFile;
        private string pathToFolder;
        private string[] filesInFolder;
        private Rectangle resolution;
        private Image picture;
        private Image pictureAncestor; // STAN PIERWOTNY ZDJECIA
        private int indexOfPresentImage = 0;
        private OpenFileDialog selectFile = new OpenFileDialog();
        private bool pictureLoaded = false;
        private FormWindowState LastWindowState = FormWindowState.Minimized;


        public s()
        {           
            InitializeComponent();
            selectFile.Title = "Select image to open";
            selectFile.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.gif) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.gif";
            selectFile.Multiselect = false;           
            resolution = Screen.PrimaryScreen.Bounds;
            left.Image = new Bitmap(left.Image, left.Size);
            right.Image = new Bitmap(right.Image, right.Size);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(selectFile.ShowDialog() == DialogResult.OK && selectFile.OpenFile() != null)
            {
                pathToFile = selectFile.FileName;
                pathToFolder = Path.GetDirectoryName(pathToFile);
                filesInFolder = (Directory.EnumerateFiles(pathToFolder, "*.*").Where(s => s.EndsWith(".jpg") || s.EndsWith(".png") || s.EndsWith(".gif") || s.EndsWith(".jpe") || s.EndsWith(".jpeg") || s.EndsWith(".jfif"))).ToArray();
                //for(int a =0;a<filesInFolder.Length;a++)
                //    MessageBox.Show(filesInFolder[a]);
                bool imageOK = true;
                try
                {
                    picture = Image.FromFile(pathToFile);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Wystąpił nieoczekiwany błąd, plik może być uszkodzony");
                    imageOK = false;
                }
                if (imageOK)
                {
                    pictureAncestor = picture;
                    if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                    {
                        picture = new Bitmap(picture, pictureBox.Size);
                    }
                    pictureBox.Image = picture;
                    pictureLoaded = true;
                    for (int a = 0; a < filesInFolder.Length; a++)
                    {
                        if (pathToFile == filesInFolder[a])
                        {
                            indexOfPresentImage = a;
                            break;
                        }
                    }
                }
            }

        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            if (pathToFile != null)
            {
                picture = pictureAncestor;
                pictureBox.Image = picture;
                if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                {
                    picture = new Bitmap(picture, pictureBox.Size);
                }
                pictureBox.Image = picture;
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState != LastWindowState)
            {
                LastWindowState = WindowState;
                if (WindowState == FormWindowState.Maximized)
                {
                    // Maximized!
                    if (pathToFile != null)
                    {
                        picture = pictureAncestor;
                        
                        if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                        {
                            picture = new Bitmap(picture, pictureBox.Size);
                        }
                        pictureBox.Image = picture;
                    }
                }
                if (WindowState == FormWindowState.Normal)
                {
                    // Restored!
                    if (pathToFile != null)
                    {
                        picture = pictureAncestor;
                        
                        if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                        {
                            picture = new Bitmap(picture, pictureBox.Size);
                        }
                        pictureBox.Image = picture;
                    }
                }
            }
        }

        private void left_Click(object sender, EventArgs e)
        {
            if(pictureLoaded && indexOfPresentImage != 0)
            {
                indexOfPresentImage--;
                bool imageOK = true;
                try
                {
                    picture = new Bitmap(filesInFolder[indexOfPresentImage]);
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Wystąpił nieoczekiwany błąd, plik może być uszkodzony");
                    imageOK = false;
                    string pathToRemove = filesInFolder[indexOfPresentImage]; //ŚCIEŻKA USZKODZONEGO PLIKU JEST USUWANA
                    filesInFolder = filesInFolder.Where(val => val != pathToRemove).ToArray();
                    indexOfPresentImage++; //PONIEWAŻ PLIK BYŁ USZKODZONY, NIE PRZESZLIŚMY DO NIEGO, WIĘC INDEKS OBECNEGO PLIKU SIĘ NIE ZMIENIA
                }
                if (imageOK)
                {
                    pictureAncestor = picture;
                    if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                    {
                        picture = new Bitmap(picture, pictureBox.Size);
                    }
                    pictureBox.Image = picture;
                }
            }
        }
        private void right_Click(object sender, EventArgs e)
        {
            if(pictureLoaded && indexOfPresentImage < filesInFolder.Length - 1)
            {
                indexOfPresentImage++;
                bool imageOK = true;
                try
                {
                    picture = new Bitmap(filesInFolder[indexOfPresentImage]);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wystąpił nieoczekiwany błąd, plik może być uszkodzony");
                    imageOK = false;
                    string pathToRemove = filesInFolder[indexOfPresentImage]; //ŚCIEŻKA USZKODZONEGO PLIKU JEST USUWANA
                    filesInFolder = filesInFolder.Where(val => val != pathToRemove).ToArray();
                    indexOfPresentImage--; //PONIEWAŻ PLIK BYŁ USZKODZONY, NIE PRZESZLIŚMY DO NIEGO, WIĘC INDEKS OBECNEGO PLIKU SIĘ NIE ZMIENIA
                }
                if (imageOK)
                {
                    pictureAncestor = picture;

                    if (picture.Size.Height > pictureBox.Size.Height || picture.Size.Width > pictureBox.Size.Width)
                    {
                        picture = new Bitmap(picture, pictureBox.Size);
                    }
                    pictureBox.Image = picture;
                }
            }
        }
    }
}
